
# Z emergency switch
Z probes make standard Z endstop switches useless, usually. However Z probes may fail, and the nozzle may crash onto the build plate, damaging it... [as it just happened](img/thank-you-nozzle.png).

This step-by-step procedure shows how to use the Z endstop as a software-driven halt switch, thus avoiding damaging the nozzle/plate in case of the Z probe malfunctioning.

*Please note that a software-driven halt switch may not provide the same reliability as a kill switch.*

![z-emergency-switch](img/z-emergency-stop.gif)

## Reference platform
This procedure refers to the platform that follows.

| 3D Printer    | Ender 3 Pro                     |
|---------------|---------------------------------|
| Motherboard   | Creality 4.4.2                  |
| Z probe      	| BLTouch 3.1                     |
| Z endstop    	| Mechanical endstop limit switch |
| Firmware     	| Marlin 2.0.x                    |

It may be still valid for other Z probes/motherboards, as long as:
 - the Z probe does not use the Z endstop pin on the motherboard,
 - the Z endstop is triggered when the nozzle hits the build plate.

## Source
You can find the ready-to-use firmware in the folder `src`.
You have to build and install it as described in the [official guide](https://marlinfw.org/docs/basics/install.html).

The detailed customization procedure is described below.

## Step 1 - Mount the Z endstop switch
Level the bed manually through the bed leveling knobs, and mount the Z endstop switch on the printer's frame. The switch has to [trigger when the nozzle hits the build plate](img/z-switch-trigger-point.png).

*Hint: you can precisely tune the trigger point by using a multimeter connected to the [S and V pins](img/z-switch-test-pins.png) of the Z endstop switch.

## Step 2 - Wire the Z endstop switch
Connect the Z endstop switch to the `Z-` socket. Polarity doesn't matter.

| Z stop switch 	| Z- socket       |
|---------------	|----------------	|
| S             	| S/V             |
| G               | unwired        	|
| V             	| V/S             |

Refer to the [creality 4.2.2 board](img/creality-422-sockets.png) image to identify the `Z-` socket.

## Step 3 - Install the Z probe
Mount the BLTouch probe, as described in the user manual. 

## Step 4 - Wire the Z probe
Connect the BLTouch probe to the `BL_T` socket as follows.

| BLTouch 	| BL_T socket     |
|---------	|----------------	|
| G       	| G              	|
| 5V      	| V              	|
| S       	| IN             	|
| G       	| G              	|
| Z-      	| OUT            	|

Refer to the [creality 4.2.2 board](img/creality-422-sockets.png) image to identify the `BL_T` socket.

## Step 5 - Download Marlin 2.0.x
Download the lates version of [Marlin 2.0.x](https://marlinfw.org/meta/download/).

## Step 6 - Edit `Configuration.h`
In `Configuration.h`, enable the Z probe to perform Z homing and specify that the probe does not use the Z endstop pin.
```
//#define Z_MIN_PROBE_USES_Z_MIN_ENDSTOP_PIN
#define USE_PROBE_FOR_Z_HOMING
```

Then, enable BLTouch as Z probe and configure it, as described in the [official guide](https://marlinfw.org/docs/configuration/configuration.html#z-probe-options).
```
#define BLTOUCH
#define NOZZLE_TO_PROBE_OFFSET { -45, -4, 0 }  // Warning: Check the values, your offset may be different! 
```
For the Z offset use your best known value and tune at runtime through the Z probe offset wizard.

It may be worth enabling safe homing also, to avoid homing with a Z probe outside the build plate area. 
However, the Z halt switch will protect the bed against such type of misconfiguration.
```
#define Z_SAFE_HOMING

#if ENABLED(Z_SAFE_HOMING)
  #define Z_SAFE_HOMING_X_POINT ((X_BED_SIZE - 10) / 2)    // X point for Z homing
  #define Z_SAFE_HOMING_Y_POINT ((Y_BED_SIZE - 10) / 2)    // Y point for Z homing
#endif
```

## Step 7 - Edit `Configuration_adv.h`
In ```Configuration_adv.h```, set the endstops to be always on.
```
#define ENDSTOPS_ALWAYS_ON_DEFAULT
```

Enable babystepping for tuning the Z offset while printing.
```
#define BABYSTEPPING
```

Then, enable the Z Offset Wizard, for manually adjusting the Z Offset after the Auto Bed Leveling.
```
// Add Probe Z Offset calibration to the Z Probe Offsets menu
  #if HAS_BED_PROBE
    #define PROBE_OFFSET_WIZARD
    #if ENABLED(PROBE_OFFSET_WIZARD)
      //
      // Enable to init the Probe Z-Offset when starting the Wizard.
      // Use a height slightly above the estimated nozzle-to-probe Z offset.
      // For example, with an offset of -5, consider a starting height of -4.
      //
      #define PROBE_OFFSET_WIZARD_START_Z -4.0

      // Set a convenient position to do the calibration (probing point and nozzle/bed-distance)
      #define PROBE_OFFSET_WIZARD_XY_POS { X_CENTER, Y_CENTER }
    #endif
  #endif
```

## Step 8 - Edit `src/pins/stm32f1/pins_CREALITY_V4.h`
In `src/pins/stm32f1/pins_CREALITY_V4.h`, set the physical Z endstop pin as the `KILL_PIN`, i.e. the pin that triggers the halt routine.
Append the following instruction at the end of the file.
```
#define KILL_PIN Z_STOP_PIN
```

## Step 9 - Edit `src/MarlinCore.h`
In `src/MarlinCore.h`, set the `KILL_PIN_STATE` to `HIGH`, since the Z endstop limit switch is normally open. The pin triggers when the switch is hit, i.e. the circuit is closed and the pin goes `HIGH`.
```
#if HAS_KILL
  #ifndef KILL_PIN_STATE
    #define KILL_PIN_STATE HIGH
  #endif
  inline bool kill_state() { return READ(KILL_PIN) == KILL_PIN_STATE; }
#endif
```

## Step 10 - Build and install the firmware
Now you can build and install the firmware as described in the [official guide](https://marlinfw.org/docs/basics/install.html).

## Step 11 - Tests
Perform the tests that follow, to cover the scenarios in which the Z emergency switch may be useful.

*Hint: you can trigger a reboot by pressing the Z endstop switch two times.*

#### Test 1
Power on.
Expected result: BLTouch self-tests and goes in ready state, i.e. steady red light, *no blinking*. Printer goes ready.

*Troubleshooting: If the BLTouch does not self-test and enters in error state (red blinking), 
then reset the probe via LCD menu, manually deploy the tip of the probe, and reset the 3D printer.*

#### Test 2
Press the Z endstop switch while printer is ready but doing nothing.\
Expected result: 3D printer halts, reboot required.

#### Test 3
Run the Z Offset Wizard and make the nozzle touches (gently) the build plate, by using negative babysteps on Z axis.\
Expected result: Z Offset Wizard halts when the nozzle hits the build plate. Reboot required.

*Troubleshooting: If the Z endstop is not triggered, adjust its mounting and/or manually re-level the bed.*

## Step 12 - Done!
Well done, your plate is safe now!

# Author
Francesco Racciatti

# License
This project is licensed under the [GNU General Public License](#LICENSE).

# Support
If you found this project useful, you may think to give me a cup of coffee.\
Thank you for your support :).

[![](https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YS32QGERBZLUN)
